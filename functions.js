var apples = 20; // Яблоки, кг
var strawberry = 20; // Клубника - 75 р/кг
var apricot = 20; // Абрикос - 35 р/кг
var flour = 20; // Мука - 10 р/кг
var milk = 20; // Молоко - 15 р/литр
var eggs = 50; // Яйца - 3 р/шт
var applePie = 0; // количество яблочных пирогов, которое испекли.
var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.


function cookingPies(n) {
	// Печем яблочные пироги:
	while (applePie < n) {
	  if (apples - 3 < 0) {
	  	console.log('Кончились яблоки! Купили еще 10 кг')
	  	apples = apples + 10;
	  } else if (flour - 2 < 0) {
	  	console.log('Кончилась мука! Купили еще 10 кг')
	  	flour = flour + 10;
	  } else if (milk - 1 < 0) {
	  	console.log('Кончилось молоко! Купили еще 10 литров')
	  	milk = milk + 10;
	  } else if (eggs - 3 < 0) {
	  	console.log('Кончились яйца! Купили еще 10 штук')
	  	eggs = eggs + 10;
	  } else {
	  apples = apples - 3;
	  flour = flour - 2;
	  milk = milk - 1;
	  eggs = eggs - 3;
	  applePie++
	  console.log('Яблочный пирог   ' +applePie+ ' готов!');
	  }
	}

	
	// Печем клубничные пироги:
	while (strawberryPie < n) {
	  if (strawberry - 5 < 0) {
	  	console.log('Кончилась клубника! Купили еще 10 кг')
	  	strawberry = strawberry + 10;
	  } else if (flour - 1 < 0) {
	  	console.log('Кончилась мука! Купили еще 10 кг')
	  	flour = flour + 10;
	  } else if (milk - 2 < 0) {
	  	console.log('Кончилось молоко! Купили еще 10 литров')
	  	milk = milk + 10;
	  } else if (eggs - 4 < 0) {
	  	console.log('Кончились яйца! Купили еще 10 штук')
	  	eggs = eggs + 10;
	  } else {
	  strawberry = strawberry - 5;
	  flour = flour - 1;
	  milk = milk - 2;
	  eggs = eggs - 4;
	  strawberryPie++
	  console.log('Клубничный пирог '+strawberryPie+' готов!');
	  }
	
	}


	// Печем абрикосовые пироги:
	while (apricotPie < n) {
	if (apricot - 2 < 0) {
	  	console.log('Кончились абрикосы! Купили еще 10 кг')
	  	apricot = apricot + 10;
	  } else if (flour - 3 < 0) {
	  	console.log('Кончилась мука! Купили еще 10 кг')
	  	flour = flour + 10;
	  } else if (milk - 2 < 0) {
	  	console.log('Кончилось молоко! Купили еще 10 литров')
	  	milk = milk + 10;
	  } else if (eggs - 2 < 0) {
	  	console.log('Кончились яйца! Купили еще 10 штук')
	  	eggs = eggs + 10;
	  } else {
	  apricot = apricot - 2;
	  flour = flour - 3;
	  milk = milk - 2;
	  eggs = eggs - 2;
	  apricotPie++
	  console.log('Абрикосовый пирог '+apricotPie+' готов!');
	  }
	}
}

cookingPies(10);


var total = 0;
function sumTo(n) {
	for (var i = 1; i <= n; i++) {
		total += i;
	}
	console.log(total);
}

sumTo(4);


function sumTo1(n) {
	if (n > 1) {
		return n + sumTo1(n - 1);
	} else {
		return n;
	}
}

console.log(sumTo1(3));



function sumTo2(n) {
  if (n == 1) return 1;
  return n + sumTo2(n-1);
}

console.log(sumTo2(5));
